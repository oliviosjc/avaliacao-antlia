﻿using AmigoProximo.Domain.Entities;
using AmigoProximo.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmigoProximo.Domain.Interfaces.Services
{
    public interface IMovimentoManualRepository : IRepositoryBase<MovimentoManual>
    {
        List<MovimentoManualListar> Listar();
    }
}
