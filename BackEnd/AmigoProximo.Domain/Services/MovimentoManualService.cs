﻿using AmigoProximo.Domain.Entities;
using AmigoProximo.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmigoProximo.Domain.Services
{
    public class MovimentoManualService : ServiceBase<MovimentoManual>, IMovimentoManualService
    {
        IMovimentoManualRepository _repository;

        public MovimentoManualService(IMovimentoManualRepository repository)
            : base(repository)
        {
            this._repository = repository;
        }

        public List<MovimentoManualListar> Listar()
        {
            return this._repository.Listar();
        }
    }
}
