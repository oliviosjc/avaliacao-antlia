﻿using AmigoProximo.Domain.Entities;
using AmigoProximo.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmigoProximo.Domain.Services
{
    public class ProdutoCosifService : ServiceBase<ProdutoCosif>, IProdutoCosifService
    {
        IProdutoCosifRepository _repository;

        public ProdutoCosifService(IProdutoCosifRepository repository)
            : base(repository)
        {
            this._repository = repository;
        }
    }
}
