﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmigoProximo.Domain.Entities
{
    public class Produto
    {

        public Produto()
        {
            ProdutoCosifs = new List<ProdutoCosif>();
        }

        public int COD_PRODUTO { get; set; }
        public string DES_PRODUTO { get; set; }
        public string STA_STATUS { get; set; }

        public virtual ICollection<ProdutoCosif> ProdutoCosifs { get; set; }
    }
}
