﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmigoProximo.Domain.Entities
{
    public class ProdutoCosif
    {

        public ProdutoCosif()
        {
            MovimentoManuais = new List<MovimentoManual>();
        }

        public int COD_PRODUTO { get; set; }
        public int COD_COSIF { get; set; }
        public string COD_CLASSIFICACAO { get; set; }
        public string STA_STATUS { get; set; }
        
        public virtual Produto Produto { get; set; }

        public virtual ICollection<MovimentoManual> MovimentoManuais { get; set; }
    }
}
