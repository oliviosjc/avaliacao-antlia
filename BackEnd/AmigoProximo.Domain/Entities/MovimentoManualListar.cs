﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmigoProximo.Domain.Entities
{
    public class MovimentoManualListar
    {

        public Int32 DAT_MES { get; set; }
        public Int32 DAT_ANO { get; set; }
        public string COD_PRODUTO { get; set; }
        public string DESC_PRODUTO { get; set; }
        public Int32 NUM_LANCAMENTO { get; set; }
        public string DES_DESCRICAO { get; set; }
        public decimal VAL_VALOR { get; set; }

    }
}
