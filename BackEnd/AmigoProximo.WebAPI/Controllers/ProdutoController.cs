﻿using AmigoProximo.Application.AppService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace AmigoProximo.WebAPI.Controllers
{
    [RoutePrefix("api/produto")]
    public class ProdutoController : ApiController
    {
        private readonly IProdutoAppService _produtoAppService;

        public ProdutoController(IProdutoAppService produtoAppService)
        {
            this._produtoAppService = produtoAppService;
        }

        [Route("obterProdutosAtivos"), HttpGet]
        public IHttpActionResult ObterProdutosAtivos()
        {
            try
            {
                var _obterProdutosAtivos = this._produtoAppService.ObterProdutosAtivos();

                return Ok(_obterProdutosAtivos);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}