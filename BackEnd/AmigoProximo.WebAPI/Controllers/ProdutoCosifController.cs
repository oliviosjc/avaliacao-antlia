﻿using AmigoProximo.Application.AppService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace AmigoProximo.WebAPI.Controllers
{
    [RoutePrefix("api/produtoCosif")]
    public class ProdutoCosifController : ApiController
    {
        private readonly IProdutoCosifAppService _produtoCosifAppService;

        public ProdutoCosifController(IProdutoCosifAppService produtoCosifAppService)
        {
            this._produtoCosifAppService = produtoCosifAppService;
        }

        [Route("obterPorProtudo/{COD_PRODUTO:int}"), HttpGet]
        public IHttpActionResult ObterPorProduto(Int32 COD_PRODUTO)
        {
            try
            {
                var _obterProdutoCofis = this._produtoCosifAppService.ObterPorProduto(COD_PRODUTO);

                return Ok(_obterProdutoCofis);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}