﻿    using AmigoProximo.Application.AppService.Interfaces;
using AmigoProximo.Application.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace AmigoProximo.WebAPI.Controllers
{
    [RoutePrefix("api/movimentoManual")]
    public class MovimentoManualController : ApiController
    {
        private readonly IMovimentoManualAppService _service;

        public MovimentoManualController(IMovimentoManualAppService service)
        {
            this._service = service;
        }

        [Route("adicionar"), HttpPost]
        public IHttpActionResult Adicionar(MovimentoManualVM model)
        {
            try
            {
                this._service.AdicionarMovimento(model);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("obter"), HttpGet]
        public IHttpActionResult Listar()
        {
            try
            {
                return Ok(this._service.Lista());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}