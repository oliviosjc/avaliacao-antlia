﻿using AmigoProximo.Application.AppService;
using AmigoProximo.Application.AppService.Interfaces;
using AmigoProximo.Domain.Interfaces.Repositories;
using AmigoProximo.Domain.Interfaces.Services;
using AmigoProximo.Domain.Services;
using AmigoProximo.Infra.Data.Context;
using AmigoProximo.Infra.Data.Repositories;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmigoProximo.Infra.CrossCutting.IoC
{
    public class BootStrapper
    {
        public static void RegisterServices(Container container)
        {
            container.Register<AmigoProximoContext>(Lifestyle.Scoped);

            #region AppService
            container.Register<IProdutoAppService, ProdutoAppService>();
            container.Register<IProdutoCosifAppService, ProdutoCosifAppService>();
            container.Register<IMovimentoManualAppService, MovimentoManualAppService>();

            #endregion

            #region 'Inject Service'

            container.Register<IProdutoService, ProdutoService>();
            container.Register<IProdutoCosifService, ProdutoCosifService>();
            container.Register<IMovimentoManualService, MovimentoManualService>();

            #endregion

            #region 'Inject Repository'

            container.Register<IProdutoRepository, ProdutoRepository>();
            container.Register<IProdutoCosifRepository, ProdutoCosifRepository>();
            container.Register<IMovimentoManualRepository, MovimentoManualRepository>();

            #   endregion
        }
    }
}
