﻿using AmigoProximo.Domain.Entities;
using AmigoProximo.Domain.Interfaces.Services;
using AmigoProximo.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmigoProximo.Infra.Data.Repositories
{
    public class MovimentoManualRepository : RepositoryBase<MovimentoManual>, IMovimentoManualRepository
    {
        AmigoProximoContext _context;
        public MovimentoManualRepository(AmigoProximoContext context) : base(context)
        {
            this._context = context;
        }

        public List<MovimentoManualListar> Listar()
        {
            return _context.Database
               .SqlQuery<MovimentoManualListar>("ObterMovimentacao")
               .ToList();
        }
    }
}
