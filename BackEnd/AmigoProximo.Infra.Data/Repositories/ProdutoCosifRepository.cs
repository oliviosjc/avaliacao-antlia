﻿using AmigoProximo.Domain.Entities;
using AmigoProximo.Domain.Interfaces.Services;
using AmigoProximo.Infra.Data.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmigoProximo.Infra.Data.Repositories
{
    public class ProdutoCosifRepository : RepositoryBase<ProdutoCosif>, IProdutoCosifRepository
    {
        public ProdutoCosifRepository(AmigoProximoContext context) : base(context)
        {

        }
    }
}
