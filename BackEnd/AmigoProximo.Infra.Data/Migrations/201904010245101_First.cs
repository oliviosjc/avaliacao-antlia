namespace AmigoProximo.Infra.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class First : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MovimentoManual",
                c => new
                    {
                        DAT_MES = c.Int(nullable: false),
                        DAT_ANO = c.Int(nullable: false),
                        NUM_LANCAMENTO = c.Int(nullable: false),
                        COD_PRODUTO = c.String(nullable: false, maxLength: 128),
                        COD_COSIF = c.String(nullable: false, maxLength: 128),
                        VAL_VALOR = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DES_DESCRICAO = c.String(maxLength: 50),
                        DAT_MOVIMENTO = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        COD_USUARIO = c.String(maxLength: 50),
                        ProdutoCosif_COD_PRODUTO = c.Int(),
                        ProdutoCosif_COD_COSIF = c.Int(),
                    })
                .PrimaryKey(t => new { t.DAT_MES, t.DAT_ANO, t.NUM_LANCAMENTO, t.COD_PRODUTO, t.COD_COSIF })
                .ForeignKey("dbo.PRODUTO_COSIF", t => new { t.ProdutoCosif_COD_PRODUTO, t.ProdutoCosif_COD_COSIF })
                .Index(t => new { t.ProdutoCosif_COD_PRODUTO, t.ProdutoCosif_COD_COSIF });
            
            CreateTable(
                "dbo.PRODUTO_COSIF",
                c => new
                    {
                        COD_PRODUTO = c.Int(nullable: false),
                        COD_COSIF = c.Int(nullable: false),
                        COD_CLASSIFICACAO = c.String(maxLength: 6, fixedLength: true, unicode: false),
                        STA_STATUS = c.String(),
                    })
                .PrimaryKey(t => new { t.COD_PRODUTO, t.COD_COSIF })
                .ForeignKey("dbo.PRODUTO", t => t.COD_PRODUTO)
                .Index(t => t.COD_PRODUTO);
            
            CreateTable(
                "dbo.PRODUTO",
                c => new
                    {
                        COD_PRODUTO = c.Int(nullable: false, identity: true),
                        DESC_PRODUTO = c.String(maxLength: 30),
                        STA_STATUS = c.String(),
                    })
                .PrimaryKey(t => t.COD_PRODUTO);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PRODUTO_COSIF", "COD_PRODUTO", "dbo.PRODUTO");
            DropForeignKey("dbo.MovimentoManual", new[] { "ProdutoCosif_COD_PRODUTO", "ProdutoCosif_COD_COSIF" }, "dbo.PRODUTO_COSIF");
            DropIndex("dbo.PRODUTO_COSIF", new[] { "COD_PRODUTO" });
            DropIndex("dbo.MovimentoManual", new[] { "ProdutoCosif_COD_PRODUTO", "ProdutoCosif_COD_COSIF" });
            DropTable("dbo.PRODUTO");
            DropTable("dbo.PRODUTO_COSIF");
            DropTable("dbo.MovimentoManual");
        }
    }
}
