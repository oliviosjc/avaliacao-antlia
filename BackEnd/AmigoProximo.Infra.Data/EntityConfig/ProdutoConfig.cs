﻿using AmigoProximo.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmigoProximo.Infra.Data.EntityConfig
{
    public class ProdutoConfig : EntityTypeConfiguration<Produto>
    {
        public ProdutoConfig()
        {
            ToTable("PRODUTO");

            HasKey(x => x.COD_PRODUTO)
               .Property(x => x.COD_PRODUTO)
               .HasColumnName("COD_PRODUTO");

            Property(e => e.DES_PRODUTO)
                .HasMaxLength(30)
               .HasColumnName("DESC_PRODUTO");

            Property(e => e.STA_STATUS)
               .HasColumnName("STA_STATUS");
        }
    }
}
