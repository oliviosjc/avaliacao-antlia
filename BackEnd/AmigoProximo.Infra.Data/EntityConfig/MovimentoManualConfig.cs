﻿using AmigoProximo.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmigoProximo.Infra.Data.EntityConfig
{
    public class MovimentoManualConfig : EntityTypeConfiguration<MovimentoManual>
    {
        public MovimentoManualConfig()
        {
            HasKey(e => new { e.DAT_MES, e.DAT_ANO, e.NUM_LANCAMENTO, e.COD_PRODUTO, e.COD_COSIF });

            Property(e => e.DES_DESCRICAO)
                .HasMaxLength(50)
               .HasColumnName("DES_DESCRICAO");

            Property(e => e.DAT_MOVIMENTO)
                .HasColumnType("smalldatetime")
               .HasColumnName("DAT_MOVIMENTO");

            Property(e => e.COD_USUARIO)
                .HasMaxLength(50)
               .HasColumnName("COD_USUARIO");

            Property(e => e.VAL_VALOR)
                .HasPrecision(18, 2)
               .HasColumnName("VAL_VALOR");
        }
    }
}
