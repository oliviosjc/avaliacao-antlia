﻿using AmigoProximo.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmigoProximo.Application.ViewModel
{
    public class ProdutoVM
    {

        public ProdutoVM(Produto produto)
        {
            this.COD_PRODUTO = produto.COD_PRODUTO;
            this.DES_PRODUTO = produto.DES_PRODUTO;
            this.STA_STATUS = produto.STA_STATUS;
            this.ProdutoCosifVM = produto.ProdutoCosifs.Select(sl => new ProdutoCosifVM(sl)).ToList();
        }

        public int COD_PRODUTO { get; set; }
        public string DES_PRODUTO { get; set; }
        public string STA_STATUS { get; set; }
        public IList<ProdutoCosifVM> ProdutoCosifVM { get; set; }
    }
}
