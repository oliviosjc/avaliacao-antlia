﻿using AmigoProximo.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmigoProximo.Application.ViewModel
{
    public class ProdutoCosifVM
    {

        public ProdutoCosifVM(ProdutoCosif produtoCosif)
        {
            this.COD_PRODUTO = produtoCosif.COD_PRODUTO;
            this.COD_COSIF = produtoCosif.COD_COSIF;
            this.COD_CLASSIFICACAO = produtoCosif.COD_CLASSIFICACAO;
            this.STA_STATUS = produtoCosif.STA_STATUS;
            this.Produto = produtoCosif.Produto;
            this.MovimentosManuais = produtoCosif.MovimentoManuais.Select(sl => new MovimentoManualVM(sl)).ToList();
        }

        public int COD_PRODUTO { get; set; }
        public int COD_COSIF { get; set; }
        public string COD_CLASSIFICACAO { get; set; }
        public string STA_STATUS { get; set; }
        public Produto Produto { get; set; }
        public IList<MovimentoManualVM> MovimentosManuais { get; set; }

    }
}
