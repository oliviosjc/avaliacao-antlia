﻿using AmigoProximo.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmigoProximo.Application.ViewModel
{
    public class MovimentoManualVM
    {

        public MovimentoManualVM()
        {

        }

        public static MovimentoManual convertToEntity(MovimentoManualVM model)
        {
            var movimento = new MovimentoManual();

            movimento.DAT_MES = model.DAT_MES;
            movimento.DAT_ANO = model.DAT_ANO;
            movimento.NUM_LANCAMENTO = model.NUM_LANCAMENTO;
            movimento.COD_PRODUTO = model.COD_PRODUTO;
            movimento.COD_COSIF = model.COD_COSIF;
            movimento.VAL_VALOR = model.VAL_VALOR;
            movimento.DES_DESCRICAO = model.DES_DESCRICAO;
            movimento.DAT_MOVIMENTO = model.DAT_MOVIMENTO;
            movimento.COD_USUARIO = model.COD_USUARIO;

            return movimento;
        }

        public MovimentoManualVM(MovimentoManual movimentoManual)
        {
            this.DAT_MES = movimentoManual.DAT_MES;
            this.DAT_ANO = movimentoManual.DAT_ANO;
            this.COD_PRODUTO = movimentoManual.COD_PRODUTO;
            this.DES_DESCRICAO = movimentoManual.DES_DESCRICAO;
            this.DES_DESCRICAO_PRODUTO = movimentoManual.ProdutoCosif.Produto.DES_PRODUTO;
            this.NUM_LANCAMENTO = movimentoManual.NUM_LANCAMENTO;
            this.VAL_VALOR = movimentoManual.VAL_VALOR;
        }

        public Int32 DAT_MES { get; set; }
        public Int32 DAT_ANO { get; set; }
        public string COD_PRODUTO { get; set; }
        public string DES_DESCRICAO { get; set; }
        public string DES_DESCRICAO_PRODUTO { get; set; }
        public Int32 NUM_LANCAMENTO { get; set; }
        public decimal VAL_VALOR { get; set; }
        public string COD_COSIF { get; set; }
        public DateTime DAT_MOVIMENTO { get; set; }
        public string COD_USUARIO { get; set; }

    }
}
