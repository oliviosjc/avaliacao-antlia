﻿using AmigoProximo.Application.AppService.Interfaces;
using AmigoProximo.Application.ViewModel;
using AmigoProximo.Domain.Entities;
using AmigoProximo.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmigoProximo.Application.AppService
{
    public class ProdutoAppService : AppServiceBase<Produto>, IProdutoAppService
    {
        private IProdutoService _service;

        public ProdutoAppService(IProdutoService service) : base(service)
        {
            this._service = service;
        }

        public List<ProdutoVM> ObterProdutosAtivos()
        {
            try
            {
                var _produtos = this._service
                    .FindAll(f => f.STA_STATUS == "A")
                    .Select(sl => new ProdutoVM(sl))
                    .ToList();

                return _produtos;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
