﻿using AmigoProximo.Application.AppService.Interfaces;
using AmigoProximo.Application.ViewModel;
using AmigoProximo.Domain.Entities;
using AmigoProximo.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmigoProximo.Application.AppService
{
    public class MovimentoManualAppService : AppServiceBase<MovimentoManual>, IMovimentoManualAppService
    {

        private IMovimentoManualService _service;

        public MovimentoManualAppService(IMovimentoManualService service) : base(service)
        {
            this._service = service;
        }

        public void AdicionarMovimento(MovimentoManualVM movimentoVM)
        {
            try
            {
                var _movimentacoes = this._service
                    .FindAll(f => f.DAT_MES == movimentoVM.DAT_MES && f.DAT_ANO == movimentoVM.DAT_ANO)
                    .OrderByDescending(or => or.NUM_LANCAMENTO)
                    .ToList();

                var _movimento = MovimentoManualVM.convertToEntity(movimentoVM);

                if (!_movimentacoes.Any())
                {
                    _movimento.NUM_LANCAMENTO = 1;
                }
                else
                {
                    _movimento.NUM_LANCAMENTO = _movimentacoes.FirstOrDefault().NUM_LANCAMENTO + 1;
                }

                _movimento.COD_USUARIO = "TESTE";
                _movimento.DAT_MOVIMENTO = DateTime.Now;
                this._service.Insert(_movimento);

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<MovimentoManualListar> Lista()
        {
            try
            {
                return this._service.Listar();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
