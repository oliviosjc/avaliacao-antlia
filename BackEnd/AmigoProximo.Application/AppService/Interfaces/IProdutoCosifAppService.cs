﻿using AmigoProximo.Application.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmigoProximo.Application.AppService.Interfaces
{
    public interface IProdutoCosifAppService
    {
        List<ProdutoCosifVM> ObterPorProduto(Int32 COD_PRODUTO);
    }
}
