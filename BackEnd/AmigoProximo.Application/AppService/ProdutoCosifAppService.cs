﻿using AmigoProximo.Application.AppService.Interfaces;
using AmigoProximo.Application.ViewModel;
using AmigoProximo.Domain.Entities;
using AmigoProximo.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmigoProximo.Application.AppService
{
    public class ProdutoCosifAppService : AppServiceBase<ProdutoCosif>, IProdutoCosifAppService
    {

        private IProdutoCosifService _service;

        public ProdutoCosifAppService(IProdutoCosifService service) : base(service)
        {
            this._service = service;
        }

        public List<ProdutoCosifVM> ObterPorProduto(Int32 COD_PRODUTO)
        {
            try
            {
                var _produtosCosif = this._service
                    .FindAll(f => f.COD_PRODUTO == COD_PRODUTO)
                    .Select(sl => new ProdutoCosifVM(sl))
                    .ToList();

                return _produtosCosif;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
