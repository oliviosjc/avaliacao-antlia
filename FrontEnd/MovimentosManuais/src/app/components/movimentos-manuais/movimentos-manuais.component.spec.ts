import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovimentosManuaisComponent } from './movimentos-manuais.component';

describe('MovimentosManuaisComponent', () => {
  let component: MovimentosManuaisComponent;
  let fixture: ComponentFixture<MovimentosManuaisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovimentosManuaisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovimentosManuaisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
