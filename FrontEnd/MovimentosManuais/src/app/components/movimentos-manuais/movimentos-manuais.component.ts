import { Component, OnInit } from '@angular/core';
import { ProdutoService } from '../../services/produto.service';
import { MovimentoManualService } from '../../services/movimento-manual.service';
import { ProdutoCosifService } from '../../services/produto-cosif.service';
import { HttpErrorResponse } from '@angular/common/http';
import { FormBuilder} from '@angular/forms';
declare var $: any;

@Component({
  selector: 'app-movimentos-manuais',
  templateUrl: './movimentos-manuais.component.html',
  styleUrls: ['./movimentos-manuais.component.css']
})
export class MovimentosManuaisComponent implements OnInit {

  produtosAtivos: any;
  movimentos: any;
  produtosCosif: any;
  produtoSelect: any;
  produtoCosifSelect: any;
  mesInput: any;
  anoInput: any;

  objeto: any = {};
  

  constructor(private _serviceProduto : ProdutoService, private _serviceMovimento : MovimentoManualService, private _serviceProdutoCosif : ProdutoCosifService,   private formBuilder: FormBuilder ) 
  {

  }

  ngOnInit() 
  {

    this.objeto.DAT_MES = '';
    this.objeto.DAT_ANO = '';
    this.objeto.COD_PRODUTO = 0;
    this.objeto.COD_COSIF = 0;
    this.objeto.VAL_VALOR = '';
    this.objeto.DES_DESCRICAO = '';

    this.desabilitarForm();
    this.obterProdutosAtivos();
    this.obterMovimentacoes();
  }

  obterProdutosAtivos()
  {
    this._serviceProduto.obterProdutosAtivos().subscribe((data) => {
    this.produtosAtivos = data;
    },
    (error: HttpErrorResponse) => {
      console.log(error);
    })
  }

  obterProdutosCosif(produtoID: any)
  {
    this._serviceProdutoCosif.obterPorProduto(produtoID).subscribe((data) => {
      this.produtosCosif = data;
    },
    (error : HttpErrorResponse) => {
      console.log(error);
    })
  }

  changeProduto()
  {
    this.obterProdutosCosif(this.objeto.COD_PRODUTO);
  }

  obterMovimentacoes()
  {
    this._serviceMovimento.obterMovimentos().subscribe((data) => {
      this.movimentos = data;
    }, 
    (error: HttpErrorResponse) => {
      console.log(error);
    })
  }

  adicionarMovimentacao(obj: any)
  {
    this._serviceMovimento.adicionarMovimento(obj).subscribe((data) => {
      this.obterMovimentacoes();
    },
    (error: HttpErrorResponse) => {
      console.log(error);
    })
  }

  novo()
  {
    $("input").prop('disabled', false);
    $("select").prop('disabled', false);
    $("textarea").prop('disabled', false);
    $("#btnIncluir").prop('disabled', false);
    $("#btnLimpar").prop('disabled', false);
  }

  desabilitarForm()
  {
    $("input").prop('disabled', true);
    $("select").prop('disabled', true);
    $("textarea").prop('disabled', true);
    $("#btnIncluir").prop('disabled', true);
    $("#btnLimpar").prop('disabled', true);
  }

  incluir(objeto: any)
  {
    this._serviceMovimento.adicionarMovimento(objeto).subscribe((data) => {
      this.obterMovimentacoes();
      this.desabilitarForm();
      this.limparObjeto();
    },
    (error: HttpErrorResponse) => {
      console.log(error);
    })
  }

  limparObjeto()
  {
    
    this.objeto.DAT_MES = '';
    this.objeto.DAT_ANO = '';
    this.objeto.COD_PRODUTO = 0;
    this.objeto.COD_COSIF = 0;
    this.objeto.VAL_VALOR = '';
    this.objeto.DES_DESCRICAO = '';

  }

}
