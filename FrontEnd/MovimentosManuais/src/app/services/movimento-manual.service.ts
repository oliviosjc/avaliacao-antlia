import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MovimentoManualService {

  constructor(private http: HttpClient) { }

  adicionarMovimento(obj : any)
  {
    return this.http.post(environment.url + '/api/movimentoManual/adicionar/', obj);
  }

  obterMovimentos()
  {
    return this.http.get(environment.url + '/api/movimentoManual/obter/');
  }

}