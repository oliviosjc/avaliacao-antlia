import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProdutoCosifService {

  constructor(private http: HttpClient) { }

  obterPorProduto(COD_PRODUTO : any)
  {
    return this.http.get(environment.url + '/api/produtoCosif/obterPorProtudo/' + COD_PRODUTO);
  }

}